class BetsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def show
    @bet = Bet.find(params[:id])
    @users = @bet.followers.pluck(:id, :name)
  end

  def index
    @bets = Bet.paginate(page: params[:page])
    render json: @bets
  end

  def followBet
    @bet = Bet.find(params[:id])
    # User.find(params[:id])
    puts("kkkkk")
    # puts(bet_params)
    puts(@bet.user_id)
    puts("Meet me in da club2")
    if current_user.followBet(@bet)
      # flash[:success] = "Bet created!"
      
      # redirect_to root_url
      render json: @bet
    else
      # render 'static_pages/home'
      render json: {
        error: "Error - didn't follow",
        status: 500
      }, status: 500
    end
  end

  def unfollowBet
    @bet = Bet.find(params[:id])
    # User.find(params[:id])
    puts("ssssss")
    # puts(bet_params)
    puts(@bet.user_id)
    puts("Meet me in da club2")
    if current_user.unfollowBet(@bet)
      # flash[:success] = "Bet created!"
      
      # redirect_to root_url
      render json: @bet
    else
      # render 'static_pages/home'
      render json: {
        error: "Error - didn't unfollow",
        status: 500
      }, status: 500
    end
  end

  def checkIfFollowing
    @bet = Bet.find(params[:id])
    # User.find(params[:id])
    puts("ffffff")
    # puts(bet_params)
    puts(@bet.user_id)
    @res = false
    if current_user.followingBet?(@bet)
      @res = true   
      render json: @res
    else
      render json: @res
    end
  end

  def create
    @bet = current_user.bets.build(bet_params)

    puts("meet me in da club1")
    puts(bet_params)
    puts(@bet.user_id)
    puts("Meet me in da club2")
    if @bet.save
      flash[:success] = "Bet created!"
      current_user.followBet(@bet)
      # redirect_to root_url
      render json: @bet
    else
      # render 'static_pages/home'
      render json: {
        error: "Error - didn't save",
        status: 500
      }, status: 500
    end
  end
  
  def destroy
  end
  
  private
  
    def bet_params
      params.require(:bet).permit(
        :title,
        :verb,
        :incOrDec,
        :benchmark,
        :moreOrLess,
        :unit,
        :dateStart,
        :dateEnd,
        :allowed_mulligans,
        :period
      )
    end
end