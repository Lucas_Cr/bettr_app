class ContractsController < ApplicationController
    before_action :logged_in_user

    def create
      # add check to see if user already has contract with bet
      @contract = Contract.where(followedBet_id: @bet.id, follower_id: current_user.id).first()
      if @contract
        render json: {
          error: "Error - already following",
          status: 500
        }, status: 500
      else
        @bet = Bet.find(params[:followedBet_id])
        current_user.followBet(@bet)
        respond_to do |format|
          format.html { redirect_to @bet}
          format.js
        end
      end
    end

    def edit
      @bet = Bet.find(params[:followedBet_id])
      @contract = Contract.where(followedBet_id: @bet.id, follower_id: current_user.id).first()
      # puts("meet me in da club1")
      # puts(bet_params)
      # puts(@contract.progress)
      # puts(contract_params[:progress] )

      # puts("Meet me in da club2")
      # params[:progress] += @contract.progress
      params[:contract][:mulligan] = contract_params[:mulligan].to_i + @contract.mulligan.to_i
      params[:contract][:progress] = contract_params[:progress].to_i + @contract.progress.to_i
      # puts(contract_params[:progress] )
      if @contract.update(contract_params)
        # flash[:success] = "Profile updated"
        render json: @contract
        # redirect_to @user
      else
        render json: {
          error: "Error - didn't save",
          status: 500
        }, status: 500
      end
      # render file: "contracts/edit.html.erb"
      # render file: "contracts/edit.html.erb", locals: {bet: @bet, contract: @contract}
      # @contract = Contract.find(params[:followedBet_id])
    end

    # def update
    #   @bet = Contract.find(params[:id]).followedBet
    #   current_user.updateContract(@bet)
    #   respond_to do |format|
    #     format.html { redirect_to current_user }
    #     format.js
    #   end
    # end

    def destroy
      @bet = Contract.find(params[:id]).followedBet
      current_user.unfollowBet(@bet)
      respond_to do |format|
        format.html { redirect_to @bet }
        format.js
      end
    end

    private

    def contract_params
      params.require(:contract).permit(
        :progress,
        # :outcome,
        :mulligan
      )
    end

end
