class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        # redirect_back_or user
        render json: user
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        # flash[:warning] = message
        render json: {
          error: message,
          status: 500
        }, status: 500
        # redirect_to root_url
      end
    else
      # flash.now[:danger] = 'Invalid email/password combination'
      # head 403
      render json: {
        error: "Invalid email/password combination!!!",
        status: 500
      }, status: 500
      # render 'new' 
    end
  end

  def destroy
    # log_out if logged_in?
    
    if logged_in?
      log_out
      message  = "Logged out"
      render json: message
    else
      message  = "Log out error"
      render json: {
        error: message,
        status: 500
      }, status: 500
    end
  end
  
  def grabUser
    message  = "Issue grabbing user"
    if logged_in?
      @user = current_user
      render json: @user
    else
      render json: {
        error: message,
        status: 500
      }, status: 500
    end
  end
end
