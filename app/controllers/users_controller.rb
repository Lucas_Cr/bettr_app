class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
  :following, :followers, :followedBets]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy


  def show
    # @user = User.find(params[:id])
    # @bets = @user.bets.paginate(page: params[:page])
    user = User.find(params[:id])
    user.bets = @user.bets.paginate(page: params[:page])  
    render json: user
  end

  def index
    @users = User.paginate(page: params[:page])
    render json: @users
  end

  def new
    @user = User.new
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def create
    # puts(user_params)
    @user = User.new(user_params)
    # puts("meet me in da club1")
    # puts(user_params)
    # puts(@user)
    # puts("Meet me in da club2")
    if @user.save
      # UserMailer.account_activation(@user).deliver_now
      @user.send_activation_email
      # flash[:info] = "Please check your email to activate your account."
      # redirect_to root_url
    else
      render json: {
        error: "Error - didn't save",
        status: 500
      }, status: 500
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      render json: @user
    else
      render json: { error: "Error - didn't update"}
    end
  end

  def following
    @title = "Following"
    # @user  = User.find(params[:id])
    @user  = User.find(params[:user][:id])
    @users = @user.following.paginate(page: params[:page])
    # render 'show_follow'
    render json: @users
  end

  def followers
    @title = "Followers"
    # @user  = User.find(params[:id])
    @user  = User.find(params[:user][:id])
    @users = @user.followers.paginate(page: params[:page])
    # render 'show_follow'
    render json: @users
  end

  def followedBets
    # @user = User.new(user_params)
    puts("meet me in da club htsce")
    puts(params[:user][:id])
    puts("meet me in da club htsce")
    @title = "Followed bets"
    @user  = User.find(params[:user][:id])
    # bets = @user.followedBets.paginate(page: params[:page])
    @bets = @user.followedBets.paginate(page: params[:page])
    puts("followedBets reached")
    # render 'show_followedBets'
    render json: @bets
  end

  def followUser
    @user = User.find(params[:id])
    # User.find(params[:id])
    # puts("meet me in da club1")
    # puts(bet_params)
    # puts(@user.user_id)
    # puts("Meet me in da club2")
    if current_user.follow(@user)
      # flash[:success] = "Bet created!"
      
      # redirect_to root_url
      render json: @user
    else
      # render 'static_pages/home'
      render json: {
        error: "Error - didn't follow",
        status: 500
      }, status: 500
    end
  end

  def unfollowUser
    @user = User.find(params[:id])
    # User.find(params[:id])
    # puts("meet me in da club1")
    # puts(bet_params)
    # puts(@user.user_id)
    # puts("Meet me in da club2")
    if current_user.unfollow(@user)
      # flash[:success] = "Bet created!"
      
      # redirect_to root_url
      render json: @user
    else
      # render 'static_pages/home'
      render json: {
        error: "Error - didn't follow",
        status: 500
      }, status: 500
    end
  end

  def checkIfFollowing
    @user = User.find(params[:id])
    # User.find(params[:id])
    # puts("meet me in da club1")
    # puts(bet_params)
    # puts(@user.user_id)
    @res = false
    if current_user.following?(@user)
      @res = true   
      render json: @res
    else
      render json: @res
    end
  end


    private
    def user_params
      params.require(:user).permit(
        :id,
        :name,
        :email,
        :password,
        :password_confirmation
      )
    end
    # # Confirms a logged-in user.
    # def logged_in_user
    #   unless logged_in?
    #     store_location
    #     flash[:danger] = "Please log in."
    #     redirect_to login_url
    #   end
    # end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
