/* eslint no-console: 0 */
// Run this example by adding <%= javascript_pack_tag 'hello_vue' %> (and
// <%= stylesheet_pack_tag 'hello_vue' %> if you have styles in your component)
// to the head of your layout file,
// like app/views/layouts/application.html.erb.
// All it does is render <div>Hello Vue</div> at the bottom of the page.


import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import 'vuetify/dist/vuetify.min.css'
import App from '../app.vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'



import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
// import './material-design-icons-iconfont/dist/material-design-icons.css' 



// import logout from './components/logout'



Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueAxios, Axios);
Vue.use(Vuex);

// const Foo = { template: '<div>foo</div>' }
// const Bar = { template: '<div>bar</div>' }

// Static pages
import About from "./components/static_pages/about.vue";
import Contact from "./components/static_pages/contact.vue";
import Help from "./components/static_pages/help.vue";
import Home from "./components/static_pages/home.vue";
import ActivationPage from "./components/static_pages/activationPage.vue";
import AwaitingActivation from "./components/static_pages/awaitingActivation.vue";

// Password_resets
import New_password_reset from "./components/password_resets/new.vue"
import Edit_password_reset from "./components/password_resets/edit.vue"
// Sessions
import New_session from "./components/sessions/new.vue"
// Users
import New_user from "./components/users/new.vue"
import Show_user from "./components/users/show.vue"
import Edit_user from "./components/users/edit.vue"
import User_index from "./components/users/index.vue"
import Find_users from "./components/users/findUsers.vue"
import User_wrapper from "./components/users/showWrapper.vue"

// Bets
import Find_bets from "./components/bets/findBets.vue"
import Bet_wrapper from "./components/bets/showWrapper.vue"
import Builder from "./components/bets/builder.vue"



import { store } from './store'

document.addEventListener('DOMContentLoaded', () => {
  const app = new Vue({
    vuetify: new Vuetify({
      icons: {
        iconfont: 'md', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
      },
      theme: {
        themes: {
          light: {
            primary: '#1f1e1e',
            secondary: '#fffce1',
            neutral: '#272727',
            accent1: '#FF652F',
            accent2: '#2bb4b0',
            error: '#ff4444',
            info: '#33b5e5',
            success: '#00C851',
            warning: '#FF652F'


            //original
          //  primary: '#538f80',
          //   secondary: '#fffce1',
          //   neutral: '#f9f8f7',
          //   accent1: 'ff6a00',
          //   accent2: '#2bb4b0',
          //   error: '#ff4444',
          //   info: '#33b5e5',
          //   success: '#00C851',
          //   warning: '#ffbb33'
          },
        },
      },
    }),
    router: new VueRouter({ routes:
      [
        { path: '/about',         component: About },
        { path: '/contact',       component: Contact },
        { path: '/help',          component: Help },
        { path: '/home',          component: Home },
        { path: '/newpassword',   component: New_password_reset },
        { path: '/editpassword',  component: Edit_password_reset},
        { path: '/login',         component: New_session },
        { path: '/user',          component: Show_user },
        { path: '/users',         component: Find_users },
        { path: '/user/show/',     component: User_wrapper },
        { path: '/signup',        component: New_user},
        { path: '/user/edit',   component: Edit_user},
        { path: '/bets',         component: Find_bets },
        { path: '/bet/show/',     component: Bet_wrapper },
        { path: '/bet/new/',     component: Builder },
        { path: '/activationPage',component: ActivationPage},
        { path: '/account_activations/:token/edit', component: AwaitingActivation }

      ]
    }),
    render: h => h(App),
    store,
  }).$mount()
  document.body.appendChild(app.$el)

  console.log(app)
})
// to be ported:
// get 'password_resets/new'
// get 'password_resets/edit'
// get 'sessions/new'
// get 'users/new'
// get '/home', to: 'static_pages#home'
// get '/help', to: 'static_pages#help'
// get '/about', to: 'static_pages#about'
// get '/contact', to: 'static_pages#contact'
// get '/signup', to: 'users#new'
// post '/signup', to: 'users#create'
// get '/login', to: 'sessions#new'
// post '/login', to: 'sessions#create'
// delete '/logout', to: 'sessions#destroy'