import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({

  state: {
    count: 0,
    globalUser: null
  },

  mutations: {
    incrementCounter (state, payload) {
      state.count += payload
    },
    saveGlobalUser (state, payload) {
        state.globalUser = payload
      }
  },

  actions: {
    incrementAction ({commit}, payload) {
      commit('incrementCounter', payload)
    },
    saveGlobalUserAction ({commit}, payload) {
        commit('saveGlobalUser', payload)
    }
  },

  getters: {
    counter (state) {
      return state.count
    },
    globalUser (state) {
        return state.globalUser
    }
  }
})