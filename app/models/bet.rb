class Bet < ApplicationRecord
  belongs_to :user
  # has_many :contracts
  # Bet stuff
  has_many :contracts, class_name:  "Contract",
  foreign_key: "followedBet_id",
  dependent:   :destroy
  has_many :followers, through: :contracts, source: :follower

  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 140 }
  validates :verb, presence: true, length: { maximum: 140 }
  # validates :incOrDec, inclusion: { in: [true, false] }
  validates :benchmark, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: -1000, less_than_or_equal_to: 1000 }
  validates :moreOrLess, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: -1, less_than_or_equal_to: 1 }
  validates :unit, presence: true, length: { maximum: 140 }
  validates :dateStart, presence: true, date: true
  validates :dateEnd, presence: true, date: { after: :dateStart }
end
