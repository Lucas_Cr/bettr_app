class Contract < ApplicationRecord
    belongs_to :follower, class_name: "User"
    belongs_to :followedBet, class_name: "Bet"
    validates :follower_id, presence: true
    validates :followedBet_id, presence: true


    def activateContract(progress)
        update_attribute(:progress,    progress)
        update_attribute(:mulligan,    0)
        update_attribute(:outcome, 0)
    end
end
