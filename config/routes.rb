Rails.application.routes.draw do
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  get 'sessions/destroy', to: 'sessions#destroy'
  get 'globalUser', to: 'sessions#grabUser'
  post 'checkIfFollowingBet', to: 'bets#checkIfFollowing'
  post 'followBet', to: 'bets#followBet'
  post 'unfollowBet', to: 'bets#unfollowBet'
  post 'users/followedBets', to: 'users#followedBets'
  post 'users/following', to: 'users#following'
  post 'users/followers', to: 'users#followers'

  post 'checkIfFollowingUser', to: 'users#checkIfFollowing'
  post 'followUser', to: 'users#followUser'
  post 'unfollowUser', to: 'users#unfollowUser'
  
  post 'contracts/edit', to: 'contracts#edit'
  post 'activate', to: 'account_activations#edit'
  get 'users/new'
  get 'users/show', to: 'users#show'
  get '/home', to: 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  post '/users/edit', to: 'users#update'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users do
    member do
      post :following, :followers, :followedBets
    end
  end
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :relationships,       only: [:create, :destroy]
  resources :bets                
  resources :contracts,           only: [:create, :edit, :destroy]

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
end
