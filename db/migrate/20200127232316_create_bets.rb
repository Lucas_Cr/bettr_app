class CreateBets < ActiveRecord::Migration[6.0]
  def change
    create_table :bets do |t|
      t.string :verb
      t.boolean :incOrDec
      t.integer :moreOrLess
      t.string :unit
      t.string :title
      t.datetime :dateEnd
      t.datetime :dateStart
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :bets, [:user_id, :created_at]
  end
end
