class AddBenchmarkToBets < ActiveRecord::Migration[6.0]
  def change
    add_column :bets, :benchmark, :integer
  end
end
