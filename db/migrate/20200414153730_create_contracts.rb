class CreateContracts < ActiveRecord::Migration[6.0]
  def change
    create_table :contracts do |t|
      t.integer :followedBet_id
      t.integer :follower_id
      t.integer :progress
      t.integer :outcome

      t.timestamps
    end
    add_index :contracts, :followedBet_id
    add_index :contracts, :follower_id
    add_index :contracts, [:follower_id, :followedBet_id], unique: true
  end
end
