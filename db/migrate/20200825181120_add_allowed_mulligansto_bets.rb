class AddAllowedMulliganstoBets < ActiveRecord::Migration[6.0]
  def change
    add_column :bets, :allowed_mulligans, :integer
  end
end
