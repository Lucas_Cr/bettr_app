class RemoveIncOrDecFromBets < ActiveRecord::Migration[6.0]
  def change

    remove_column :bets, :incOrDec, :boolean
  end
end
