require 'test_helper'

class BetsControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @bet = bets(:orange)
  end


  test "should redirect create when not logged in" do
    assert_no_difference 'Bet.count' do
      post bets_path, params: { bet: {  
        verb: "MyString",
        incOrDec: true,
        moreOrLess: 1,
        unit: "MyString",
        title: "MyString",
        dateEnd: 3.days.from_now,
        dateStart: 1.days.from_now,
        created_at: 2.hours.ago
        } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Bet.count' do
      delete bet_path(@bet)
    end
    assert_redirected_to login_url
  end
end
