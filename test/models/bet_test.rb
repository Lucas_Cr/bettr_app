require 'test_helper'

class BetTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @bet = @user.bets.build(title: "test title", verb: "meditate", incOrDec:true, benchmark: 5, unit: "Minutes", moreOrLess: 1, user_id: @user.id, dateStart: 1.days.from_now,  dateEnd: 8.days.from_now)
  end

  test "should be valid" do
    assert @bet.valid?
  end

  test "user id should be present" do
    @bet.user_id = nil
    assert_not @bet.valid?
  end

  test "content should be present" do
    @bet.title = "   "
    assert_not @bet.valid?
  end

  test "content should be at most 140 characters" do
    @bet.title = "a" * 141
    assert_not @bet.valid?
  end

  test "unit should be present" do
    @bet.unit = "   "
    assert_not @bet.valid?
  end

  test "unit should be at most 140 characters" do
    @bet.unit = "a" * 141
    assert_not @bet.valid?
  end

  test "verb should be present" do
    @bet.verb = "   "
    assert_not @bet.valid?
  end

  test "verb should be at most 140 characters" do
    @bet.verb = "a" * 141
    assert_not @bet.valid?
  end

  test "moreOrLess should be present" do
    @bet.moreOrLess= "   "
    assert_not @bet.valid?
  end

  test "moreOrLess should be greater than or equal to -1" do
    @bet.moreOrLess = -2
    assert_not @bet.valid?
  end

  test "moreOrLess should be less than or equal to 1" do
    @bet.moreOrLess = 2
    assert_not @bet.valid?
  end

  test "benchmark should be present" do
    @bet.moreOrLess= "   "
    assert_not @bet.valid?
  end

  test "benchmark should be greater than or equal to -1000" do
    @bet.moreOrLess = -1001
    assert_not @bet.valid?
  end

  test "benchmark should be less than or equal to 1000" do
    @bet.moreOrLess = 1001
    assert_not @bet.valid?
  end

  test "order should be most recent first" do
    assert_equal bets(:most_recent), Bet.first
  end

  test "dateStart is a valid date" do
    @bet.dateStart = "100x20c12"
    assert_not @bet.valid?
  end

  test "endDate is after startDate" do
    @bet.dateStart = 2.days.from_now
    @bet.dateEnd = 1.days.from_now
    assert_not @bet.valid?
  end
end
